using Core;
using Veldrid;

namespace Modules
{
    public class InputModule : IModule
    {
        private static InputSnapshot _inputSnapshot;

        public static InputSnapshot InputSnapshot => _inputSnapshot;

        public void Initialize()
        {
            
        }

        public void Update()
        {
            _inputSnapshot = Modulen.SdlWindow.PumpEvents();
            // while (SDL.SDL_PollEvent(out _sdlEvent) != 0)
            // {
            //     switch (_sdlEvent.type)
            //     {
            //         case SDL.SDL_EventType.SDL_KEYDOWN:
            //             switch (_sdlEvent.key.keysym.sym)
            //             {
            //                 // TODO Remove hard coded Q for quit
            //                 case SDL.SDL_Keycode.SDLK_q:
            //                     Application.Instance.Running = false;
            //                     break;
            //             }
            //             break;
            //     }
            // }
        }

        public void Deconstruct()
        {

        }
    }
}