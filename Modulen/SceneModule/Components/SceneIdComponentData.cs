using System.Collections.Generic;
using Core;

namespace Modules.Components
{
    public class SceneIdComponentData : ComponentData
    {
        public List<List<int>> Entity;

        public SceneIdComponentData()
        {
            Count = 0;
            
            Entity = new List<List<int>>();
        }
    }
}