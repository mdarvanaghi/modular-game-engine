using System.Collections.Generic;
using System.Numerics;
using Core;
using GlmNet;

namespace Modules.Components
{
    public class Transform2DComponentData : ComponentData
    {
        [InspectorAttributes.DontSerialize]
        public int[] ParentId;
        [InspectorAttributes.DontSerialize]
        public int[] FirstChildId;
        [InspectorAttributes.DontSerialize]
        public int[] PreviousSiblingId;
        [InspectorAttributes.DontSerialize]
        public int[] NextSiblingId;

        public vec2[] Position;
        public float[] Rotation;
        public vec2[] Scale;

        public Transform2DComponentData(int capacity)
        {
            Count = 0;
            
            Entity = new int[capacity];
            
            ParentId = new int[capacity];
            FirstChildId = new int[capacity];
            PreviousSiblingId = new int[capacity];
            NextSiblingId = new int[capacity];

            Position = new vec2[capacity];
            Rotation = new float[capacity];
            Scale = new vec2[capacity];
        }
    }
}