using System.Collections.Generic;
using Core;

namespace Modules
{
    public class EntityManager
    {
        // An Entity is just an ID
        private int _nextId = 0;
        private List<Entity> _entities;
        
        public int Count => _entities.Count;

        public EntityManager()
        {
            _entities = new List<Entity>();
        }

        public Entity CreateEntity()
        {
            Entity entity = new Entity(_nextId++);
            _entities.Add(entity);
            return entity;
        }

        public void DestroyEntity(Entity entity)
        {
            foreach (IComponentSystem componentSystem in Modulen.Instance.ComponentSystems)
            {
                if (componentSystem.GetComponentId(entity.Id) != -1)
                {
                    componentSystem.DestroyComponent(entity);
                }
            }
        }
    }
}