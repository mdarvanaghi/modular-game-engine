using System.Collections.Generic;
using Core;

namespace Modules
{
    public class SceneManager
    {
        public List<int> ActiveScenes { get; private set;  }

        public SceneManager()
        {
            ActiveScenes = new List<int>();
        }

        public void LoadScene(int sceneId)
        {
            if (ActiveScenes.Contains(sceneId))
            {
                Logger.LogWarning("Scene already loaded.");
                return;
            }
            ActiveScenes.Add(sceneId);
            // TODO: Implement scene loading
            // Instantiate all entities and all components that belong to this scene
        }

        public void UnloadScene(int sceneId)
        {
            if (!ActiveScenes.Contains(sceneId))
            {
                Logger.LogWarning("Scene not loaded.");
                return;
            }
            ActiveScenes.Remove(sceneId);
            // TODO: Implement scene unloading
            // Destroy all entities and all components that belong to this scene
        }
    }
}