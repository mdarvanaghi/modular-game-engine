using System.Collections.Generic;
using System.Xml.Schema;
using Core;
using Modules.Components;

namespace Modules
{
    public class SceneIdSystem : IComponentSystem
    {
        private SceneIdComponentData _sceneIdComponentData;
        public ComponentData ComponentData => _sceneIdComponentData;

        public int SceneCount => _sceneIdComponentData.Count;

        public SceneIdSystem()
        {
            _sceneIdComponentData = new SceneIdComponentData();
        }

        public Entity GetEntity(int componentId, int sceneId)
        {
            return new Entity(_sceneIdComponentData.Entity[sceneId][componentId]);
        }

        public Entity GetEntity(int componentId)
        {
            // Not possible
            return Core.Constants.Nulltity;
        }

        public int CreateComponent(Entity entity)
        {
            CreateComponent(entity, 0);
            return -1;
        }

        public int GetComponentId(int entityId)
        {
            // Not relevant
            return -1;
        }

        public void CreateComponent(Entity entity, int sceneId)
        {
            if (_sceneIdComponentData.Entity.Count < sceneId + 1)
            {
                for (int i = 0; i < sceneId + 1; i++)
                {
                    _sceneIdComponentData.Entity.Add(new List<int>());
                }
            }
            _sceneIdComponentData.Entity[sceneId].Add(entity.Id);
        }

        public void Initialize()
        {
            Modulen.Instance.ComponentSystems.Add(this);
        }


        public int GetSceneId(int entityId)
        {
            for (int sceneId = 0; sceneId < _sceneIdComponentData.Entity.Count; sceneId++)
            {
                List<int> entityList = _sceneIdComponentData.Entity[sceneId];
                foreach (int t in entityList)
                {
                    if (t == entityId)
                    {
                        return sceneId;
                    }
                }
            }
            return -1;
        }

        public int[] GetEntityIds(int sceneId)
        {
            return _sceneIdComponentData.Entity[sceneId].ToArray();
        }
        
        public void DestroyComponent(Entity entity)
        {
            int sceneId = GetSceneId(entity.Id);
            if (sceneId >= 0) DestroyComponent(sceneId);
        }

        public void DestroyComponent(int sceneId)
        {
            // TODO: Implement sceneid component destruction
            // Get last component
            int lastId = _sceneIdComponentData.Count - 1;
            // Replace destroyed component with last component
            _sceneIdComponentData.Entity[sceneId] = _sceneIdComponentData.Entity[lastId];
            _sceneIdComponentData.Count--;
        }

        public void Update()
        {
            // Nothing
        }

        public void Deconstruct()
        {
            // Todo: Clean up
        }
    }
}