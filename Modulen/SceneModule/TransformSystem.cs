using System;
using System.Numerics;
using System.Reflection;
using Core;
using GlmNet;
using Modules.Components;

namespace Modules
{
    public class TransformSystem : IComponentSystem
    {
        public enum TransformDimensions
        {
            Two,
            Three
        }
        private Transform2DComponentData _transform2DComponentData;
        public ComponentData ComponentData => _transform2DComponentData;

        public int Count => _transform2DComponentData.Count;

        public TransformSystem(TransformDimensions dimensions = TransformDimensions.Two)
        {
            if (dimensions == TransformDimensions.Three)
            {
                // TODO: Implement 3D
//                _transform3DComponentData = new Transform3DComponentData(Core.Constants.ComponentSystemCapacity);
                return;
            }
            _transform2DComponentData = new Transform2DComponentData(Core.Constants.ComponentSystemCapacity);
        }

        public Entity GetEntity(int componentId)
        {
            return new Entity(_transform2DComponentData.Entity[componentId]);
        }

        public int CreateComponent(Entity entity)
        {
            return CreateComponent(entity, Core.Constants.Nulltity, new vec2(0, 0), 0f, new vec2(0, 0));
        }
        
        public int CreateComponent(Entity entity, Entity parent)
        {
            return CreateComponent(entity, parent, new vec2(0, 0), 0f, new vec2(0, 0));
        }

        public int CreateComponent(Entity entity, Entity parent, vec2 position, float rotation, vec2 scale)
        {
            int id = _transform2DComponentData.Count++;
            _transform2DComponentData.Entity[id] = entity.Id;
            
            _transform2DComponentData.ParentId[id] = parent.Id;
            _transform2DComponentData.FirstChildId[id] = -1;
            _transform2DComponentData.PreviousSiblingId[id] = -1; // TODO Find prev sibling
            _transform2DComponentData.NextSiblingId[id] = -1; // TODO Find next sibling

            _transform2DComponentData.Position[id] = position;
            _transform2DComponentData.Rotation[id] = rotation;
            _transform2DComponentData.Scale[id] = scale;

            return id;
        }

        public int GetComponentId(int entityId)
        {
            for (int i = 0; i < Count; i++)
            {
                if (_transform2DComponentData.Entity[i] == entityId)
                {
                    return i;
                }
            }
            return -1;
        }

        public void DestroyComponent(Entity entity)
        {
            int componentId = GetComponentId(entity.Id);
            if (componentId >= 0) DestroyComponent(componentId);
        }

        public void DestroyComponent(int componentId)
        {
            // TODO: Implement transform component destruction
        }

        public int GetParent(int componentId)
        {
            return _transform2DComponentData.ParentId[componentId];
        }
        
        public int GetFirstChild(int componentId)
        {
            return _transform2DComponentData.FirstChildId[componentId];
        }
        
        public int GetNextSibling(int componentId)
        {
            return _transform2DComponentData.NextSiblingId[componentId];
        }
        
        public int GetPreviousSibling(int componentId)
        {
            return _transform2DComponentData.PreviousSiblingId[componentId];
        }

        public vec2 GetLocalPosition(int componentId)
        {
            return _transform2DComponentData.Position[componentId];
        }
        
        public float GetLocalRotation(int componentId)
        {
            return _transform2DComponentData.Rotation[componentId];
        }

        public vec2 GetLocalScale(int componentId)
        {
            return _transform2DComponentData.Scale[componentId];
        }
        
        public vec2 GetWorldPosition(int componentId)
        {
            // TODO: Implement hierarchy
            return _transform2DComponentData.Position[componentId];
        }
        
        public float GetWorldRotation(int componentId)
        {
            // TODO: Implement hierarchy
            return _transform2DComponentData.Rotation[componentId];
        }
        
        public vec2 GetWorldScale(int componentId)
        {
            // TODO: Implement hierarchy
            return _transform2DComponentData.Scale[componentId];
        }

        public void Initialize()
        {
            Modulen.Instance.ComponentSystems.Add(this);
        }

        public void Update()
        {
            // TODO: Sort scene graph if any dirty transforms
        }

        public void Deconstruct()
        {
            // TODO: Clean up
        }
    }
}