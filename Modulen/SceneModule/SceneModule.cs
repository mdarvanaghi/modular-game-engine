using Core;

namespace Modules
{
    public class SceneModule : IModule
    {
        public SceneManager SceneManager { get; private set; }
        public EntityManager EntityManager { get; private set; }
        public TransformSystem TransformSystem { get; private set; }
        public SceneIdSystem SceneIdSystem { get; private set; }

        public SceneModule()
        {
            SceneManager = new SceneManager();
            EntityManager = new EntityManager();
            TransformSystem = new TransformSystem();
            SceneIdSystem = new SceneIdSystem();
        }

        public void Initialize()
        {
            TransformSystem.Initialize();
            SceneIdSystem.Initialize();
        }

        public void Update()
        {
            
        }

        public void Deconstruct()
        {
            
        }
    }
}