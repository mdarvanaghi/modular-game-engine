﻿using System;
using Modules;
using Core;
using Veldrid;

namespace TestApplication
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Initializing application... ");
            
            // Create modules
//            IAssetManager[] assetManagers;
//            assetManagers = new IAssetManager[]
//            {
////                textureManagerSdl
//            };
            Modules.InputModule inputModule = new Modules.InputModule();
            SceneModule sceneModule = new SceneModule();
            Modules.GuiModule guiModule = new Modules.GuiModule();
            RenderModule renderModule = new RenderModule();
//            AssetManagingModule assetManagingModule = new AssetManagingModule(assetManagers);
            
            IModule[] modules;
            modules = new IModule[]
            {
                inputModule,
                sceneModule,
                renderModule,
                guiModule
            };
            Modulen modulen = new Modulen("Test Application", modules);
            Logger.Log("Done.");

            modulen.Run(); // Mainloop
            
            Console.Write("Terminating application... ");
            Logger.Log("Done.");
        }
    }
}