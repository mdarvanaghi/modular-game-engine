using System.Collections.Generic;
using Core;
using ImGuiNET;
using Modules;
using Constants = Core.Constants;

public class SceneGraphWindow : ModulenScript
{
    private SceneModule _sceneModule;
    private TransformSystem _transformSystem;
    private SceneIdSystem _sceneIdSystem;

    private ImGuiTreeNodeFlags _treeNodeFlags = ImGuiTreeNodeFlags.OpenOnArrow |
                                                ImGuiTreeNodeFlags.OpenOnDoubleClick;

    private static Entity _selectedEntity = Constants.Nulltity;

    public static Entity SelectedEntity
    {
        get => _selectedEntity;
        set => _selectedEntity = value;
    }

    private void Initialize()
    {
        _sceneModule = Modulen.GetModule<SceneModule>();
        _transformSystem = _sceneModule.TransformSystem;
        _sceneIdSystem = _sceneModule.SceneIdSystem;
    }
    
    private void RenderGui()
    {
        ShowAllSceneGraphs();
    }

    private void ShowAllSceneGraphs()
    {
        ImGui.Begin("Scene Graph");
        foreach (int activeScene in _sceneModule.SceneManager.ActiveScenes)
        {
            ShowSceneGraph(activeScene);
            ImGui.Separator();
        }
        ImGui.End();
    }

    private void ShowSceneGraph(int sceneId)
    {
        ImGui.Text("Scene " + sceneId);
        int[] entityIds = _sceneIdSystem.GetEntityIds(sceneId);

        List<int> transformSystemComponents = new List<int>();
        List<int> entitiesWithoutTransforms = new List<int>();
        foreach (int entityId in entityIds)
        {
            int transformComponent = _transformSystem.GetComponentId(entityId);
            if (transformComponent != -1) transformSystemComponents.Add(transformComponent);
            else entitiesWithoutTransforms.Add(entityId);
        }
        ImGui.BeginGroup();
        foreach (int i in entitiesWithoutTransforms)
        {
            if (ImGui.Button("Entity name goes here."))
            {
                _selectedEntity = new Entity(i);
            }
        }
        ImGui.EndGroup();
        ImGui.BeginGroup();
        foreach (int i in transformSystemComponents)
        {
            ImGui.PushID(i);
            Entity entity   = _transformSystem.GetEntity(i);
            int parent      = _transformSystem.GetParent(i);
            int firstChild  = _transformSystem.GetFirstChild(i);
            int nextSibling = _transformSystem.GetNextSibling(i);
            int prevSibling = _transformSystem.GetPreviousSibling(i);

            Entity parentEntity  = parent == -1 ? Constants.Nulltity : _transformSystem.GetEntity(parent);
            Entity firstChildEntity = firstChild == -1 ? Constants.Nulltity : _transformSystem.GetEntity(firstChild);
            Entity nextSiblingEntity = nextSibling == -1 ? Constants.Nulltity : _transformSystem.GetEntity(nextSibling);
            Entity prevSiblingEntity = prevSibling == -1 ? Constants.Nulltity : _transformSystem.GetEntity(prevSibling);

//
//            DebugNameComp entity_n = debug_name_system->lookup(entity);
//
//            DebugNameComp parent_n = debug_name_system->lookup(parent_e);
//
//            DebugNameComp first_child_n = debug_name_system->lookup(first_child_e);
//
//            DebugNameComp next_sibling_n = debug_name_system->lookup(next_sibling_e);
//
//            DebugNameComp prev_sibling_n = debug_name_system->lookup(prev_sibling_e);


//            string entity_name = entity_n.id == 0
//                                          ? "Entity " + to_string(entity.id)
//                                          : debug_name_system->get_name(entity_n);
//            string parent_name =
//                parent_n.id == 0 ? "Parent " + to_string(parent_e.id)
//                                 : debug_name_system->get_name(parent_n) + " " +
//                                       to_string(parent_e.id);
//            string first_child_name =
//                first_child_n.id == 0
//                    ? "Child " + to_string(first_child_e.id)
//                    : debug_name_system->get_name(first_child_n) + " " +
//                          to_string(first_child_e.id);
//            string next_sibling_name =
//                next_sibling_n.id == 0
//                    ? "Next " + to_string(next_sibling_e.id)
//                    : debug_name_system->get_name(next_sibling_n) + " " +
//                          to_string(next_sibling_e.id);
//            string prev_sibling_name =
//                prev_sibling_n.id == 0
//                    ? "Prev " + to_string(prev_sibling_e.id)
//                    : debug_name_system->get_name(prev_sibling_n) + " " +
//                          to_string(prev_sibling_e.id);

            ImGui.Text(i.ToString());
            ImGui.SameLine();
            bool nodeOpen;
            if (entity == _selectedEntity)
            {
                nodeOpen = ImGui.TreeNodeEx("Entity name goes here", _treeNodeFlags | ImGuiTreeNodeFlags.Selected);
            }
            else
            {
                nodeOpen = ImGui.TreeNodeEx("Entity name goes here", _treeNodeFlags);
            }
            bool hovered = ImGui.IsItemHovered();
            if (ImGui.IsItemClicked())
            {
                _selectedEntity = entity;
            }

            // start
            if (!nodeOpen && hovered)
            {
                ImGui.BeginTooltip();
            }

            // content
            if (nodeOpen || hovered)
            {
//                if (parent == -1)
//                    ImGui.Text("Parent name");
//                else if (ImGui.Button("Parent name"))
//                {
//                    _selectedEntity = parentEntity;
//                }
//
//                if (firstChildEntity.Id == -1)
//                    ImGui.Text("First child name");
//                else if (ImGui.Button("First child name"))
//                {
//                    _selectedEntity = firstChildEntity;
//                }
//
//                if (nextSiblingEntity.Id == -1)
//                    ImGui.Text("Next sibling name");
//                else if (ImGui.Button("Next sibling name"))
//                {
//                    _selectedEntity = nextSiblingEntity;
//                }
//
//                if (prevSiblingEntity.Id == -1)
//                    ImGui.Text("Prev sibling name");
//                else if (ImGui.Button("Prev sibling name"))
//                {
//                    _selectedEntity = prevSiblingEntity;
//                }
            }

            // end
            if (nodeOpen)
            {
                ImGui.TreePop();    
            }
            if (!nodeOpen && hovered)
                ImGui.EndTooltip();

            ImGui.PopID();
        }
        ImGui.EndGroup();
    }
}