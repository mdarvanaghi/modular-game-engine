using System;
using System.Collections.Generic;
using Core;
using Modules;
using ImGuiNET;

public class MainMenu : ModulenScript
{
    public static Dictionary<string, bool> OpenWindows = new Dictionary<string, bool>();
    private string clickedElement = String.Empty;
    private void RenderGui()
    {
        if (ImGui.BeginMainMenuBar())
        {
            if (ImGui.BeginMenu("File"))
            {
                ShowFileMenu();
                ImGui.EndMenu();
            }
            if (ImGui.BeginMenu("Edit"))
            {
                if (ImGui.MenuItem("Undo", "CTRL+Z")) {}
                if (ImGui.MenuItem("Redo", "CTRL+Y")) {}  // Disabled item
                ImGui.Separator();
                if (ImGui.MenuItem("Cut", "CTRL+X")) {}
                if (ImGui.MenuItem("Copy", "CTRL+C")) {}
                if (ImGui.MenuItem("Paste", "CTRL+V")) {}
                ImGui.EndMenu();
            }
            if (ImGui.BeginMenu("Windows"))
            {
                ShowWindowsMenu();
                ImGui.EndMenu();
            }
            ImGui.EndMainMenuBar();
        }
    }

    private void ShowFileMenu()
    {
        
    }

    private void ShowWindowsMenu()
    {
        foreach (KeyValuePair<string, bool> window in OpenWindows)
        {
            if (ImGui.MenuItem(window.Key))
            {
                clickedElement = window.Key;
            }
        }
        if (clickedElement != String.Empty)
            OpenWindows[clickedElement] = !OpenWindows[clickedElement];
        clickedElement = String.Empty;
    }
}