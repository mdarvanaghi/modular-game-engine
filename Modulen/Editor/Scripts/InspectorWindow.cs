using System;
using System.Reflection;
using Modules;
using ImGuiNET;
using Core;

public class InspectorWindow : ModulenScript
{
    private void Initialize()
    {
        CollectComponentSystems();
    }

    private void CollectComponentSystems()
    {
        
    }

    private void RenderGui()
    {
        ImGui.ShowDemoWindow();
        ImGui.Begin("Inspector");
        foreach (IComponentSystem system in Modulen.Instance.ComponentSystems)
        {
            int compId = system.GetComponentId(SceneGraphWindow.SelectedEntity.Id);
            if (compId == -1) continue;
            SerializeComponent(system, compId);
        }
        ImGui.End();
    }

    private void SerializeComponent(IComponentSystem componentSystem, int id)
    {
        ImGui.Text(SceneGraphWindow.SelectedEntity.ToString());
        ImGui.Text(componentSystem + ": " + id);
        // Todo: Do this once per recompilation, not every damn frame like a frickin cowboy
        // Maybe add 
        foreach (FieldInfo fieldInfo in componentSystem.ComponentData.GetType().GetRuntimeFields())
        {
            if (fieldInfo.GetCustomAttribute(typeof(InspectorAttributes.DontSerialize)) != null)
            {
                continue;
            }
            Array valueArray = fieldInfo.GetValue(componentSystem.ComponentData) as Array;
            // Supported types:
            // Bool, Int, Float, String
            // Note: Can't switch a type, unfortunately.
            Type elementType = valueArray.GetType().GetElementType();
            if (elementType == typeof(bool))
            {
                bool[] boolArray = valueArray as bool[];
                ImGui.Checkbox(fieldInfo.Name, ref boolArray[id]);
            }
            else if (elementType == typeof(float))
            {
                float[] floatArray = valueArray as float[];
                ImGui.DragFloat(fieldInfo.Name, ref floatArray[id]);
            }
            else if (elementType == typeof(int))
            {
                int[] intArray = valueArray as int[];
                ImGui.DragInt(fieldInfo.Name, ref intArray[id]);
            }
            else if (elementType == typeof(string))
            {
                string[] stringArray = valueArray as string[];
                ImGui.InputText(fieldInfo.Name, ref stringArray[id], 64);
            }
            else
            {
                ImGui.Text(fieldInfo.Name + ": " + valueArray.GetValue(id));
            }
        }
    }

    private void OnApplicationRecompiled()
    {
        CollectComponentSystems();
    }
}
