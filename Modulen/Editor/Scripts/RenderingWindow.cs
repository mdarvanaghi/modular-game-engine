using System.Reflection;
using Core;
using Modules;
using ImGuiNET;
using Veldrid;

public class RenderingWindow : ModulenScript
{
    private RenderModule _renderModuleRef;
    private static bool open = true;
    
    private void Initialize()
    {
        _renderModuleRef = Modulen.GetModule<RenderModule>();
        MainMenu.OpenWindows["Rendering"] = open;
    }

    private void RenderGui()
    {
        ImGui.Begin("Rendering", ref open);
        // Show all options for the Veldrid graphics device
        GraphicsDeviceFeatures gdfeatures = _renderModuleRef.GraphicsDevice.ResourceFactory.Features;
        foreach (PropertyInfo property in gdfeatures.GetType().GetProperties())
        {
            bool value = (bool) property.GetValue(gdfeatures);
            ImGui.Checkbox(property.Name, ref value);
        }
        ImGui.End();
    }
}
