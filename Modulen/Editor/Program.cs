﻿using Core;
using ImGuiNET;
using Modules;

namespace Editor
{
    class Program
    {
        static void Main(string[] args)
        {
            InputModule inputModule = new InputModule();
            SceneModule sceneModule = new SceneModule();
            RenderModule renderModule = new RenderModule();
            GuiModule guiModule = new GuiModule();
            SpriteModule spriteModule = new SpriteModule();
            ScriptingModule scriptingModule = new ScriptingModule();

            IModule[] modules =
            {
                inputModule,
                sceneModule,
                renderModule,
                guiModule,
                spriteModule,
                scriptingModule
            };
            
            /* DEBUG */
            // TODO: Load this stuff in with the scenemodule

            guiModule.RenderGuiCalls += () =>
            {
                ImGui.Begin("Controls");
                if (ImGui.Button("Compile"))
                {
                    scriptingModule.RecompileScripts();
                }
                if (ImGui.Button("Run"))
                {
                    scriptingModule.RecompileScripts();
                    scriptingModule.RunScripts();   
                }
                ImGui.End();
            };

            for (int i = 0; i < 10; i++)
            {
                Entity entity = sceneModule.EntityManager.CreateEntity();
                int componentId = sceneModule.TransformSystem.CreateComponent(entity);
                sceneModule.SceneIdSystem.CreateComponent(entity, 0);
                
            }

            for (int i = 0; i < 3; i++)
            {
                Entity entity = sceneModule.EntityManager.CreateEntity();
                int componentId = sceneModule.TransformSystem.CreateComponent(entity, new Entity(0));
                sceneModule.SceneIdSystem.CreateComponent(entity, 0);
            }
            sceneModule.SceneManager.LoadScene(0);

            /* DEBUG */
            Modulen modulen = new Modulen("Modulen Editor", modules);
        }
    }
}