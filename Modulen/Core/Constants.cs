namespace Core
{
    public static class Constants
    {
        public static readonly Entity Nulltity = new Entity(-1);
        public static readonly int ComponentSystemCapacity = 8 * 512;
    }
}