using System;

namespace Core
{
    public static class InspectorAttributes
    {
        public class Serialize : Attribute
        {
        }
        public class DontSerialize : Attribute
        {
        }
    }
}