using System.Collections.Generic;

namespace Core
{
    public class ComponentData
    {
        [InspectorAttributes.DontSerialize]
        public int Count;
        [InspectorAttributes.DontSerialize]
        public int[] Entity;
    }
}