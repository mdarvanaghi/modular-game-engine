﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Core;
using Veldrid.Sdl2;
using Veldrid.StartupUtilities;

namespace Core
{
    public class Modulen
    {
        #region Public Fields

        public static string CurrentApplicationName;
        public static string RuntimeDirectory;
        public IModule[] EngineModules;
        public List<IComponentSystem> ComponentSystems;

        private int _framesRan = 0;
        public static long TimeSinceStart => _instance._stopwatch.ElapsedMilliseconds;
        public static float DeltaTime => (float) (_instance._stopwatch.ElapsedMilliseconds - _instance._lastTime);

        #endregion

        #region Singleton Pattern
        private static Modulen _instance;
        private static readonly object Padlock = new object();

        public static Modulen Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new Modulen("", null));
                }
            }
        }
        #endregion

        #region Private Fields

        private Stopwatch _stopwatch;
        private static Sdl2Window _sdlWindow;
        private double _lastTime;

        public static Sdl2Window SdlWindow => _sdlWindow;

        #endregion

        public Modulen(string currentApplicationName, IModule[] modules)
        {
            _instance = this;
            
            CurrentApplicationName = currentApplicationName;
            RuntimeDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            EngineModules = modules;
            ComponentSystems = new List<IComponentSystem>();
            
            WindowCreateInfo windowCI = new WindowCreateInfo
            {
                X = Sdl2Native.SDL_WINDOWPOS_CENTERED,
                Y = Sdl2Native.SDL_WINDOWPOS_CENTERED,
                WindowWidth = 1028,
                WindowHeight = 800,
                WindowTitle = currentApplicationName
            };
            
            _sdlWindow = VeldridStartup.CreateWindow(windowCI);
            _stopwatch = new Stopwatch();

            foreach (IModule module in EngineModules)
            {
                module.Initialize();
            }
            Run();
        }

        public void Run()
        {
            _stopwatch.Start();
            while (_sdlWindow.Exists)
            {
                Update();
            }
            Terminate();
        }

        private void Update()
        {
            foreach (IModule module in EngineModules)
            {
                module.Update();
            }
            foreach (IComponentSystem componentSystem in ComponentSystems)
            {
                componentSystem.Update();
            }
            _framesRan++;
            _lastTime = _stopwatch.ElapsedMilliseconds;
        }

        public void Terminate()
        {
            Sdl2Native.SDL_DestroyWindow(_sdlWindow.SdlWindowHandle);
        }

        ~Modulen()
        {
            foreach (IModule module in EngineModules)
            {
                module.Deconstruct();
            }
            EngineModules = null;
            _instance = null;
        }

        public static T GetModule<T>()
        {
            foreach (IModule module in _instance.EngineModules)
            {
                if (module.GetType() == typeof(T))
                    return (T) module;
            }
            throw new Exception("Module does not exist.");
        }
        
        public static T GetComponentSystem<T>()
        {
            foreach (IComponentSystem componentSystem in _instance.ComponentSystems)
            {
                if (componentSystem.GetType() == typeof(T))
                    return (T) componentSystem;
            }
            throw new Exception("Component system does not exist.");
        }
    }
}