namespace Core
{
    public class Singleton<T>
    {
        private static Singleton<T> instance;
        private static readonly object padlock = new object();

        public static Singleton<T> Instance
        {
            get
            {
                lock (padlock)
                {
                    return instance ?? (instance = new Singleton<T>());
                }
            }
        }
    }
}