namespace Core
{
    public interface IModule
    {
        void Initialize();
        void Update();
        void Deconstruct();
    }
}