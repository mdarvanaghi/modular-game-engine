namespace Core
{
    public interface IComponentSystem
    {
        ComponentData ComponentData { get; }
        void Initialize();
        Entity GetEntity(int componentId);
        int CreateComponent(Entity entity);
        int GetComponentId(int entityId);
        void DestroyComponent(Entity entity);
        void DestroyComponent(int componentId);
        void Update();
        void Deconstruct();
    }
}