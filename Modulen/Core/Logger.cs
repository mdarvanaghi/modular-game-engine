using System;

namespace Core
{
    public static class Logger
    {
        public static void Log(string message)
        {
            Console.WriteLine(message);
        }
        
        public static void Log(string message, params object[] args)
        {
            Console.WriteLine(message, args);
        }
        
        public static void Log(object obj)
        {
            Log(obj.ToString());
        }

        public static void LogWarning(string warning)
        {
            Log(warning);
        }
        
        public static void LogWarning(string warning, params object[] args)
        {
            Log(warning, args);
        }
        
        public static void LogWarning(object obj)
        {
            Log(obj.ToString());
        }

        public static void LogError(string error)
        {
            Log(error);
        }
        
        public static void LogError(string error, params object[] args)
        {
            Log(error, args);
        }

        public static void LogError(object obj)
        {
            Log(obj.ToString());
        }
    }
}