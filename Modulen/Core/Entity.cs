namespace Core
{
    public class Entity
    {
        public readonly int Id;
        public Entity(int id)
        {
            Id = id;
        }

        public Entity(int id, int scene)
        {
            Id = id;
        }

        public static bool operator== (Entity a, Entity b)
        {
            return a?.Id == b?.Id;
        }
        
        public static bool operator!= (Entity a, Entity b)
        {
            return a?.Id != b?.Id;
        }

        public override string ToString()
        {
            return "Entity " + Id;
        }
    }
}