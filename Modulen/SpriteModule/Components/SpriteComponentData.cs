using System.Numerics;
using Core;
using Veldrid;

namespace Modules.Components
{
    public class SpriteComponentData : ComponentData
    {
        public Texture[] AlbedoMap;
        public Texture[] NormalMap;

        public SpriteComponentData(int capacity)
        {
            AlbedoMap = new Texture[capacity];
            NormalMap = new Texture[capacity];
        }
    }
}