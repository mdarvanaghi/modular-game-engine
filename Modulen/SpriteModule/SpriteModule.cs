﻿using Core;

namespace Modules
{
    public class SpriteModule : IModule
    {
        private SpriteSystem _spriteSystem;

        public void Initialize()
        {
            _spriteSystem = new SpriteSystem();
        }

        public void Update()
        {
            throw new System.NotImplementedException();
        }

        public void Deconstruct()
        {
            throw new System.NotImplementedException();
        }
    }
}