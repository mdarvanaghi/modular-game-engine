using Core;
using Modules.Components;
using Veldrid;

namespace Modules
{
    public class SpriteSystem : IComponentSystem
    {
        private SpriteComponentData _spriteComponentData;
        public ComponentData ComponentData => _spriteComponentData;
        
        public int Count => _spriteComponentData.Count;
        
        public void Initialize()
        {
            _spriteComponentData = new SpriteComponentData(Core.Constants.ComponentSystemCapacity);
        }

        public Entity GetEntity(int componentId)
        {
            return new Entity(_spriteComponentData.Entity[componentId]);
        }

        public int CreateComponent(Entity entity)
        {
            return CreateComponent(entity, null); // TODO: Pass "missing texture"-texture here
        }

        public int CreateComponent(Entity entity, Texture albedoMap)
        {
            int id = _spriteComponentData.Count++;
            _spriteComponentData.Entity[id] = entity.Id;
            _spriteComponentData.AlbedoMap[id] = albedoMap;
            _spriteComponentData.NormalMap[id] = null; // TODO: Implement normal maps

            return id;
        }

        public int GetComponentId(int entityId)
        {
            for (int i = 0; i < Count; i++)
            {
                if (_spriteComponentData.Entity[i] == entityId)
                {
                    return i;
                }
            }
            return -1;
        }

        public void DestroyComponent(Entity entity)
        {
            throw new System.NotImplementedException();
        }

        public void DestroyComponent(int componentId)
        {
            throw new System.NotImplementedException();
        }

        public void Update()
        {
            throw new System.NotImplementedException();
        }

        public void Deconstruct()
        {
            throw new System.NotImplementedException();
        }
    }
}