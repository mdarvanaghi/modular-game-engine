using System.Collections.Generic;
using Core;
using Veldrid;

namespace Modules
{
    public static partial class AssetManager
    {
        private static Dictionary<string, Texture> _loadedTextures = new Dictionary<string, Texture>();
        private static RenderModule _renderModuleRef;
        
        static partial void Initialize()
        {
            _renderModuleRef = Modulen.GetModule<RenderModule>();
        }
        
        private static bool LoadTexture(string path)
        {
            _renderModuleRef.GraphicsDevice.
            return true;
        }

        public static void GetTexture(string path)
        {
            if (!_loadedTextures.ContainsKey(path))
            {
                LoadTexture(path);
            }
        }
    }
}