using System;
using Core;
using Modules;
using Veldrid;

namespace Modules
{
    public class GuiModule : IModule
    {
        private RenderModule _renderModuleRef;
        private GraphicsDevice _graphicsDeviceRef;

        private ImGuiRenderer _imGuiRenderer;
        private CommandList _imCommandList;

        public Action RenderGuiCalls;
        public Action UserRenderGuiCalls;

        public void Initialize()
        {
            _renderModuleRef = Modulen.GetModule<RenderModule>();
            _graphicsDeviceRef = _renderModuleRef.GraphicsDevice;
            _imCommandList = RenderModule.CommandList;

            _imGuiRenderer = new ImGuiRenderer(
                _graphicsDeviceRef,
                _graphicsDeviceRef.MainSwapchain.Framebuffer.OutputDescription,
                Modulen.SdlWindow.Width,
                Modulen.SdlWindow.Height);

            Modulen.SdlWindow.Resized += () =>
            {
                _imGuiRenderer.WindowResized(Modulen.SdlWindow.Width, Modulen.SdlWindow.Height);
            };

            _renderModuleRef.RenderCalls += DrawImGui; // Hook into draw call in Render Module
        }

        public void Update()
        {

        }

        public void AddUserRenderGuiCall(Delegate call)
        {
            UserRenderGuiCalls += (Action) call;
        }

        public void ClearUserRenderGuiCalls()
        {
            UserRenderGuiCalls = null;
        }

        private void DrawImGui()
        {
            _imGuiRenderer.Update(1f/60f, InputModule.InputSnapshot);    
            _imGuiRenderer.Update(Modulen.DeltaTime, InputModule.InputSnapshot);
            RenderGuiCalls?.Invoke();
            UserRenderGuiCalls?.Invoke();
            _imGuiRenderer.Render(_graphicsDeviceRef, _imCommandList);
        }

        public void Deconstruct()
        {
            _imGuiRenderer.Dispose();
        }
    }
}
