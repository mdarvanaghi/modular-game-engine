using System.Reflection;
using System.Runtime.Loader;


namespace Modules
{
    public class CollectibleAssemblyLoadContext : AssemblyLoadContext
    {
        public CollectibleAssemblyLoadContext() : base(isCollectible: true)
        {
        }
        protected override Assembly Load(AssemblyName name)
        {
            return null;
        }
    }
}