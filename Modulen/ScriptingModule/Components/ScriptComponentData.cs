using System.Collections.Generic;
using Core;

namespace Modules.Components
{
    public class ScriptComponentData : ComponentData
    {
        public string[] ScriptPath;

        public ScriptComponentData(int capacity)
        {
            Count = 0;
            
            Entity = new int[capacity];
            
            ScriptPath = new string[capacity];
        }
    }
}