using System;
using Core;
using Modules.Components;

namespace Modules
{
    public class ScriptSystem : IComponentSystem
    {
        private ScriptComponentData _scriptComponentData;
        public ComponentData ComponentData => _scriptComponentData;

        public ScriptSystem()
        {
            _scriptComponentData = new ScriptComponentData(Core.Constants.ComponentSystemCapacity);
        }
        
        public void Initialize()
        {
            Modulen.Instance.ComponentSystems.Add(this);
        }

        public Entity GetEntity(int componentId)
        {
            return new Entity(_scriptComponentData.Entity[componentId]);
        }

        public int CreateComponent(Entity entity)
        {
            return CreateComponent(entity, "NewScript.cs"); // Todo: Find existing new files and increment script number
        }

        public int CreateComponent(Entity entity, string scriptPath)
        {
            int id = _scriptComponentData.Count++;
            _scriptComponentData.Entity[id] = entity.Id;
            
            _scriptComponentData.ScriptPath[id] = scriptPath;

            return id;
        }

        public void CompileScripts()
        {
            
        }

        public int GetComponentId(int entityId)
        {
            for (int i = 0; i < _scriptComponentData.Count; i++)
            {
                if (_scriptComponentData.Entity[i] == entityId)
                {
                    return i;
                }
            }
            return -1;
        }

        public void DestroyComponent(Entity entity)
        {
            int componentId = GetComponentId(entity.Id);
            if (componentId >= 0) DestroyComponent(componentId);
        }

        public void DestroyComponent(int ComponentId)
        {
            throw new System.NotImplementedException();
        }

        public void Update()
        {
            
        }

        public void Deconstruct()
        {
            throw new System.NotImplementedException();
        }
    }
}