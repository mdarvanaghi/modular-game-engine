using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using Core;

namespace Modules
{
    public class ScriptingModule : IModule
    {
        private string _dllName;
        private GuiModule _guiModuleReference;
        private RenderModule _renderModuleReference;
        private CollectibleAssemblyLoadContext _assemblyLoadContext;
        
        public static string ScriptsDirectory;
        public bool ScriptsCanCompile;
        public ScriptSystem ScriptSystem { get; private set; }
        public Action AssemblyRecompiled;
        
        public ScriptingModule()
        {
            ScriptSystem = new ScriptSystem();
            Logger.Log(Assembly.GetEntryAssembly()?.Location);
        }
        
        public void Initialize()
        {
            _guiModuleReference = Modulen.GetModule<GuiModule>();
            _renderModuleReference = Modulen.GetModule<RenderModule>();
            ScriptsDirectory = Path.Combine(Modulen.RuntimeDirectory, "Scripts");
            ScriptSystem.Initialize();
            _dllName = Modulen.CurrentApplicationName.Replace(" ", ".") + ".dll";
        }
        
        public void RecompileScripts()
        {
            if (_assemblyLoadContext != null)
            {
                CleanUpAssembly();
            }
            using (ScriptCompiler scriptCompiler = new ScriptCompiler())
            {
                ScriptsCanCompile = scriptCompiler.CompileScripts(_dllName);
                if (!ScriptsCanCompile)
                {
                    Logger.LogError("Could not compile scripts. Check error log.");
                }
                scriptCompiler.Dispose();
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private WeakReference LoadInContextExecuteAndUnload()
        {
            CollectibleAssemblyLoadContext assemblyLoadContext = new CollectibleAssemblyLoadContext();
            Assembly scriptsAssembly = assemblyLoadContext.LoadFromAssemblyPath(Path.GetFullPath(_dllName));
            
            ExecuteAssembly(scriptsAssembly);
            _guiModuleReference.ClearUserRenderGuiCalls();
            _renderModuleReference.ClearUserRenderCalls();
            assemblyLoadContext.Unload();
            // TODO: Remove all assembly calls - otherwise the AssemblyLoadContext will not be collected
            return new WeakReference(assemblyLoadContext);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public void RunScriptsOnce()
        {
            WeakReference alcReference = LoadInContextExecuteAndUnload();
            for (int i = 0; i < 8 && alcReference.IsAlive; i++)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

            if (alcReference.IsAlive)
            {
                Logger.LogError("Could not unload assembly.");
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public void RunScripts()
        {
            _assemblyLoadContext = new CollectibleAssemblyLoadContext();
            Assembly scriptsAssembly = _assemblyLoadContext.LoadFromAssemblyPath(Path.GetFullPath(_dllName));

            ExecuteAssembly(scriptsAssembly);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public void CleanUpAssembly()
        {
            _guiModuleReference.ClearUserRenderGuiCalls();
            _renderModuleReference.ClearUserRenderCalls();
            _assemblyLoadContext?.Unload();
            WeakReference alcRef = new WeakReference(_assemblyLoadContext);
            _assemblyLoadContext = null;
            
            for (int i = 0; i < 100 && alcRef.IsAlive; i++)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }

            if (alcRef.IsAlive)
            {
                Logger.LogError("Could not unload assembly.");
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private void ExecuteAssembly(Assembly assembly)
        {
            AssemblyRecompiled?.Invoke();
            try
            {
                List<ModulenScript> scriptInstances = new List<ModulenScript>();
                foreach (Type type in assembly.GetTypes())
                {
                    if (type.BaseType == typeof(ModulenScript))
                    {
                        scriptInstances.Add((ModulenScript) assembly.CreateInstance(type.ToString()));
                    }
                }

                foreach (ModulenScript scriptInstance in scriptInstances)
                {
                    Type scriptType = scriptInstance.GetType();
                    scriptType.GetMethod("Initialize")?.Invoke(scriptInstance, null);
                    MethodInfo guiMethodInfo = scriptType.GetMethod("RenderGui");
                    MethodInfo renderMethodInfo = scriptType.GetMethod("OnRender");
                    if (guiMethodInfo != null)
                    {
                        _guiModuleReference.AddUserRenderGuiCall(Delegate.CreateDelegate(typeof(Action), scriptInstance, guiMethodInfo));
                    }

                    if (renderMethodInfo != null)
                    {
                        _renderModuleReference.AddUserRenderCall(Delegate.CreateDelegate(typeof(Action), scriptInstance, renderMethodInfo));                        
                    }
                }
//                MethodInfo entry = assembly.EntryPoint;
//
//                object result = entry.GetParameters().Length > 0 ?
//                    entry.Invoke(null, new object[] { new string[] {Modulen.CurrentApplicationName}  }) :
//                    entry.Invoke(null, null);
            }
            catch (TargetInvocationException e)
            {
                Logger.LogError(e.Message);
                Logger.LogError(e.Source);
            }
        }

        public void Update()
        {

        }

        public void Deconstruct()
        {

        }
    }
}
