using System;
using System.Reflection;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Core;
using Modules;

namespace Modules
{
    public class ScriptCompiler : IDisposable
    {
        private readonly string _dotnetCoreDirectory;
        private string _dllName;

        private List<SyntaxTree> _syntaxTrees;
        private List<MetadataReference> _references;
        
        private EmitResult _emitResult;

        public ScriptCompiler()
        {
            _dotnetCoreDirectory = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory();

            _syntaxTrees = new List<SyntaxTree>();
            _references = new List<MetadataReference>();
        }
        
        public bool CompileScripts(string dllName)
        {
            _dllName = dllName;
            // Make sure we purge the old dll
            File.Delete(Path.Combine(ScriptingModule.ScriptsDirectory, _dllName));
            // Load all scripts
            foreach (string script in Directory.GetFiles(ScriptingModule.ScriptsDirectory, "*.cs", SearchOption.AllDirectories))
            {
                _syntaxTrees.Add(CSharpSyntaxTree.ParseText(File.ReadAllText(script)));
            }
            AddReferences();
            return CompileToDll();
        }

        private void AddReferences()
        {
            // Add references to .Net stuff
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(_dotnetCoreDirectory, "mscorlib.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(_dotnetCoreDirectory, "netstandard.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(_dotnetCoreDirectory, "System.Runtime.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(_dotnetCoreDirectory, "System.Collections.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(_dotnetCoreDirectory, "System.Private.CoreLib.dll")));
//            _references.Add(MetadataReference.CreateFromFile(Path.Combine(_dotnetCoreDirectory, "System.Numerics.Vectors.dll")));

            // Add references to Engine stuff. But only the necessary modules.
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "Modulen.Core.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "Modulen.GuiModule.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "Modulen.ScriptingModule.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "Modulen.SceneModule.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "Modulen.RenderModule.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "Veldrid.dll")));
            _references.Add(MetadataReference.CreateFromFile(Path.Combine(Modulen.RuntimeDirectory, "ImGui.NET.dll")));

            // TODO: Load all managed dlls
            // string[] dllReferences =
            //     Directory.GetFiles(Environment.CurrentDirectory, "*.dll", SearchOption.AllDirectories);
            // foreach (string reference in dllReferences)
            // {
            //     references.Add(MetadataReference.CreateFromFile(Path.Combine(Environment.CurrentDirectory, reference)));
            // }
        }

        private bool CompileToDll()
        {
            CSharpCompilation compilation = CSharpCompilation.Create(Modulen.CurrentApplicationName)
                .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                .AddReferences(_references.ToArray())
                .AddSyntaxTrees(_syntaxTrees.ToArray());

            // Debug output
            foreach (Diagnostic compilerMessage in compilation.GetDiagnostics())
                Logger.Log(compilerMessage.GetMessage(), compilerMessage.GetHashCode());
            
            _emitResult = compilation.Emit(_dllName);

            return _emitResult.Success;
        }

        public void Dispose()
        {
            _syntaxTrees = null;
            _references = null;
            _emitResult = null;
        }
    }
}