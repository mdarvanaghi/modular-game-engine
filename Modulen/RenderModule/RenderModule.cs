using System;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Text;
using Core;
using Veldrid;
using Veldrid.SPIRV;
using Veldrid.StartupUtilities;

namespace Modules
{
    public class RenderModule : IModule
    {
        private GraphicsDevice _graphicsDevice;

        private static CommandList _commandList;
        private static DeviceBuffer _vertexBuffer;
        private static DeviceBuffer _indexBuffer;
        private static Shader[] _shaders;
        private static Pipeline _pipeline;

        private readonly string _windowName;
        private string _sdlError;
        
        private string ShadersDirectory;
        
        public GraphicsDevice GraphicsDevice => _graphicsDevice;
        public static CommandList CommandList => _commandList;
        public event Action RenderCalls;
        private event Action UserRenderCalls;

        public void Initialize()
        {
            ShadersDirectory = Path.Combine(Modulen.RuntimeDirectory, "Shaders");
            _graphicsDevice = VeldridStartup.CreateGraphicsDevice(Modulen.SdlWindow);

            CreateResources();
            Modulen.SdlWindow.Resized += () =>
            {
                _graphicsDevice.MainSwapchain.Resize((uint)Modulen.SdlWindow.Width, (uint)Modulen.SdlWindow.Height);
            };
        }

        private void CreateResources()
        {
            ResourceFactory factory = _graphicsDevice.ResourceFactory;
            
            /* TEST CODE */
             VertexPositionColor[] quadVertices =
            {
                new VertexPositionColor(new Vector2(-.75f, .75f), RgbaFloat.Red),
                new VertexPositionColor(new Vector2(.75f, .75f), RgbaFloat.Green),
                new VertexPositionColor(new Vector2(-.75f, -.75f), RgbaFloat.Blue),
                new VertexPositionColor(new Vector2(.75f, -.75f), RgbaFloat.Yellow)
            };
            ushort[] quadIndices = { 0, 1, 2, 3 };
            
            _vertexBuffer = factory.CreateBuffer(new BufferDescription(4 * VertexPositionColor.SizeInBytes, BufferUsage.VertexBuffer));
            _indexBuffer = factory.CreateBuffer(new BufferDescription(4 * sizeof(ushort), BufferUsage.IndexBuffer));
            
            _graphicsDevice.UpdateBuffer(_vertexBuffer, 0, quadVertices);
            _graphicsDevice.UpdateBuffer(_indexBuffer, 0, quadIndices);
            
            VertexLayoutDescription vertexLayout = new VertexLayoutDescription(
    new VertexElementDescription("Position", VertexElementSemantic.TextureCoordinate, VertexElementFormat.Float2),
                    new VertexElementDescription("Color", VertexElementSemantic.TextureCoordinate, VertexElementFormat.Float4));

            ShaderDescription vertexShaderDesc = new ShaderDescription(
            ShaderStages.Vertex,
                    Encoding.UTF8.GetBytes(File.ReadAllText(Path.Combine(ShadersDirectory, "Vertex.glsl"))),
            "main");
            
            ShaderDescription fragmentShaderDesc = new ShaderDescription(
            ShaderStages.Fragment,
                    Encoding.UTF8.GetBytes(File.ReadAllText(Path.Combine(ShadersDirectory, "Fragment.glsl"))),
                    "main");
            _shaders = factory.CreateFromSpirv(vertexShaderDesc, fragmentShaderDesc);
            
            GraphicsPipelineDescription pipelineDescription = new GraphicsPipelineDescription();
            pipelineDescription.BlendState = BlendStateDescription.SingleOverrideBlend;
            pipelineDescription.DepthStencilState = new DepthStencilStateDescription(
                depthTestEnabled: true,
                depthWriteEnabled: true,
                comparisonKind: ComparisonKind.LessEqual);
            pipelineDescription.RasterizerState = new RasterizerStateDescription(
                cullMode: FaceCullMode.Back,
                fillMode: PolygonFillMode.Solid,
                frontFace: FrontFace.Clockwise,
                depthClipEnabled: true,
                scissorTestEnabled: false);
            pipelineDescription.PrimitiveTopology = PrimitiveTopology.TriangleStrip;
            pipelineDescription.ResourceLayouts = System.Array.Empty<ResourceLayout>();
            pipelineDescription.ShaderSet = new ShaderSetDescription(
                vertexLayouts: new VertexLayoutDescription[] { vertexLayout },
                shaders: new Shader[] { _shaders[0], _shaders[1] });
            pipelineDescription.Outputs = _graphicsDevice.SwapchainFramebuffer.OutputDescription;
            
            _pipeline = factory.CreateGraphicsPipeline(pipelineDescription);

            _commandList = factory.CreateCommandList();
            /* TEST CODE */
            
            Logger.Log("Using rendering backend: " + _graphicsDevice.BackendType);
        }

        public void AddUserRenderCall(Delegate call)
        {
            UserRenderCalls += (Action) call;
        }

        public void ClearUserRenderCalls()
        {
            UserRenderCalls = null;
        }

        public void Update()
        {
            if (Modulen.SdlWindow.Exists)
            {
                Draw();
            }
        }

        private void Draw()
        {
            _commandList.Begin();
            _commandList.SetFramebuffer(_graphicsDevice.SwapchainFramebuffer);
            _commandList.ClearColorTarget(0, 
                new RgbaFloat(
                    Constants.DefaultClearColorNormalized[0], 
                    Constants.DefaultClearColorNormalized[1], 
                    Constants.DefaultClearColorNormalized[2], 
                    Constants.DefaultClearColorNormalized[3])
                );
            _commandList.SetVertexBuffer(0, _vertexBuffer);
            _commandList.SetIndexBuffer(_indexBuffer, IndexFormat.UInt16);
            _commandList.SetPipeline(_pipeline);
            _commandList.DrawIndexed(
                indexCount: 4,
                instanceCount: 1,
                indexStart: 0,
                vertexOffset: 0,
                instanceStart: 0);

            RenderCalls?.Invoke(); // Invoke internal engine render calls
            UserRenderCalls?.Invoke(); // Invoke all user-registered render calls
            
            _commandList.End();
            _graphicsDevice.SubmitCommands(_commandList);
            _graphicsDevice.SwapBuffers();
        }

        public void Deconstruct()
        {
            _graphicsDevice.WaitForIdle();
            _pipeline.Dispose();
            foreach (Shader shader in _shaders)
            {
                shader.Dispose();
            }
            _commandList.Dispose();
            _vertexBuffer.Dispose();
            _indexBuffer.Dispose();
            _graphicsDevice.Dispose();
        }
    }
}