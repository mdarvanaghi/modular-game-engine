using Veldrid;

namespace Modules
{
    public static partial class Constants
    {
        public static readonly byte[] DefaultClearColorRgb = {55, 73, 79, 255};
        public static readonly float[] DefaultClearColorNormalized = {55f/255f, 73f/255f, 79f/255f, 255f/255f};
    }
}